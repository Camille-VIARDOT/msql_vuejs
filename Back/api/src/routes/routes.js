const express = require('express');
const con = require('../database/db');
const jwt = require('jsonwebtoken');
const secret = require('../config').secret;
const bcrypt = require('bcrypt');
const saltRounds = 10;
const router = express.Router();

//Permet d'insérer dans la table un nouvel utilisateur 
router.post("/sign-up", (req, res) => {
    try {
        bcrypt.hash(req.body.password, saltRounds, function (err, hash) {
            let newCompte = `INSERT INTO users (name, email, password) VALUES ('${req.body.name}', '${req.body.email}', '${hash}')`;
            con.query(`SELECT * FROM users WHERE email = '${req.body.email}'`, (err, result) => {
                if (result.length) res.status(400).json('Cette email existe déjà');
                else con.query(newCompte, (error, resultat) => { 
                    if (resultat == true) res.status(200).send('validé')});
            })
        })
    } catch (error) {
        console.log(error);
    }

})

router.post("/sign-in", (req, res) => {
    try {
        con.query(`SELECT * FROM users WHERE email = '${req.body.email}'`, (err, result) => {
            if (result.length) bcrypt.compare(req.body.password, result[0].password, function (err, resulta) {
                let token = jwt.sign({ id: result[0].id, name: result[0].name, email: result[0].email}, secret, { expiresIn: 86400 });
                if (resulta == true) res.status(200).send({ auth: true, token: token });
                else res.status(401).json('Sorry, we don\'t know this user')
            });
            else res.status(401).json('Sorry, we don\'t know this user')
        })
    } catch (error) {
        console.log(error);
    }
})

router.post('/dashboard', (req, res) => {
    try {
        let del = `DELETE FROM users WHERE email = '${req.body.email}'`;
        con.query(`SELECT * FROM users WHERE email = '${req.body.email}'`, (err, result) => {
            if (result.length) bcrypt.compare(req.body.password, result[0].password, function (err, resulta) {
                if (resulta == true)
                    con.query(del, function (err, result) { res.send('Au revoir :)') });
                else res.status(401).json('Sorry, we don\t know this uers');
            });
        })
    } catch (error) {
        console.log(error);
    } 
})

router.post('/newcontact', (req, res) => {
    try {
        let newContact = `INSERT INTO contacts (name, email, user_affiliate) VALUES ('${req.body.name}', '${req.body.email}', '${req.body.user_affiliate}')`;
        con.query(newContact, (err, result) => {
            if (result == true) res.status(200).json('validé')
        })
        
    } catch (error) {
        console.log(error);
    }
})

router.get('/getcontacts/:email', (req,res) => {
    try {
        con.query(`SELECT * FROM users INNER JOIN contacts ON users.email = contacts.user_affiliate WHERE users.email = '${req.params.email}'`, (err, result) => {
            if (err) throw err 
            res.json(result)
        })
    } catch (error) {
        console.log(error);
    }
})


module.exports = router;