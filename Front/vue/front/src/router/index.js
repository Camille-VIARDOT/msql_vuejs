import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Dashboard from "../views/Dashboard.vue"
import store from "../store"

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home

    },
    {
      path: "/dashboard",
      name: "Dashboard",
      component: Dashboard,
      meta: {
        requiresAuth: true,
      },
    }
  ]
});

router.beforeEach((to, from, next) => {

  const loggedIn = store.getters.getToken;

  if (to.matched.some(route => route.meta.requiresAuth == true)) {
    if (loggedIn) {
      next();
    } else if (loggedIn == null) {
      next('/')
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router;